angular.module('starter.filters', [])
.filter('html_filters', function($sce) {
    return function(text) {
        var htmlObject = document.createElement('div');
        htmlObject.innerHTML = text;          
        return $sce.trustAsHtml(htmlObject.outerHTML);
    }
});
